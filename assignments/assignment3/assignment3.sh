#!/bin/bash
count1='*.tex'
echo "log start" > Report.txt

for entry  in $count1
do
	pdflatex $entry
done

Count2='*.pdf'

for entry in $Count2
do
	wc -w  $entry >> Report.txt
done

rm *.log
rm *.aux

