import sys
import glob

def Count(input):
    Person = []
    Hours = 0
    Minutes = 0
    for line in input.readlines():
        Person.append(line)
    name = Person[0]
    name = name.strip() 
    Person = Person[1:]
    for line in Person:
        day, hour, minute = line.split()    
        hour = int(hour[:1])
        Hours = Hours + hour
    minute = int(minute[:2])
    Minutes = Minutes + minute
    hour, Minutes = divmod(Minutes, 60)
    Hours = Hours + hour
    return name, Hours, Minutes


file = open("Timereport","w")
Texts = glob.glob("*.txt")
for item in Texts:
    TimeData = open(item,"r")
    Name, Hours, Minutes = Count(TimeData)
    Full  = "Time for : " + Name + " : " + str(Hours) + " hours & " + str(Minutes) + " minutes \n"
    file = open("Timereport","a")
    file.write(Full)
    file.close()

