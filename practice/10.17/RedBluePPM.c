#include <stdio.h>
#include <stdlib.h>
/* 
 *    This is a c to create a red and blue plain pixel map. 
 *    Usage:

      RedBluePPM outFileName numrows numCols

      here our program takes in an outfile name, and two arguments
      for the number of rows and columns for the size of the image to produce.
*/


int main(int argc,     // (argument count = argc) This is the number of things passed into this function.
         char *argv[]  // This is the array of things passed.
        ){


    int numRows;  /* place holder for the number of rows */
    int numcols;  // Place holder for the number of columns

    printf("=================================\n"); 
    printf("I am making a Plain Pixel Map!   \n");
    printf("=================================\n\n");

    if(argc!=4){   // if arguments passed to me are not equal to 
        printf("Usage: ./RedBluePPM OUTfilename numrow numcols \n");
        exit(1);
    }
    if ((numRows = atoi(argv[2])) <= 0){   //atoi = ascii to integer
         printf("Error: numRows needs to be positive");
    }

    return 0;
}

