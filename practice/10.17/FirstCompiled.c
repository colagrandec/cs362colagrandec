#include <stdio.h>
#include <stdlib.h>
/* 
 *    This is a c program that does something cool. 
 *    Showing how to make many comments
*/


int main(int argc,     // (argument count = argc) This is the number of things passed into this function.
         char *argv[]  // This is the array of things passed.
        ){

    printf("=============\n"); // This is an in line comment
    printf("Hello Casey  \n");
    printf("=============\n\n");

    return 0;
}

