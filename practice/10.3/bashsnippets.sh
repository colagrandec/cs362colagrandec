#!/bin/bash

# viewing environment variables
# echo "The value of the home variable is: "
# echo $HOME

# issue a command
#echo "The output of the pwd command is: "
#pwd

# thats boring, grab output and make it readable
#echo "The value of the pwd command is $(pwd)"

# assign command output to a variable
output=$(pwd)
echo "The value of the output variable is ${output}"

