procedure MyPumpkin(x):int,y:int,size:int)

const myColor:int := 113

%draw top
Draw.ThickLine(x+round(size/2),y+size,x+round(size*0.6),y+round(size*1.1),round(size/20),green)

%draw bottom half
Draw.FillArc(x+round(size/2),y+round(size/2),round(size/2),round(size/4),180,360,myColor)
%draw top half
Draw.FillArc(x+round(size/2),y+round(size/2),round(size/2),round(size/2),0,180,myColor)

%outline
Draw.Arc(x+round(size/2),y+round(size/2),round(size/2),round(size/4),180,360,black)
Draw.Arc(x+round(size/2),y+round(size/2),round(size/2),round(size/2),0,180,black)

%draw mouth
Draw.FillOval(x+round(size/2),y+round(size/5*2),round(size/4),round(size/10),black)
Draw.FillOval(x+round(size/2),y+round(size/5*2.5),round(size/2),round(size/10),myColor)

%draw teeth
Draw.ThickLine(x+round(size/5*2),y+round(size/2.5),x+round(size/5*2),y+round(size/2.5),round(size/10),myColor)
Draw.ThickLine(x+round(size/5*3),y+round(size/2.5),x+round(size/5*3),y+round(size/2.5),round(size/10),myColor)

%draw left eye
Draw.FillOval(x+round(size/4),y+round(size/3)*2,round(size/10),round(size/10),black)
Draw.FillOval(x+round(size/4)+round(size/15),y+round(size/3)*2-round(size/15),round(size/15),round(size/15),myColor)


%draw right eye
Draw.FillOval(x+round(size/4)*3,y+round(size/3)*2,round(size/10),round(size/10),black)
Draw.FillOval(x+round(size/4)*3-round(size/15),y+round(size/3)*2-round(size/15),round(size/15),round(size/15),myColor)


end MyPumpkin
