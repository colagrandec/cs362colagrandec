import sys
import os
import math
from string import whitespace


Class Address:
	def __init__(self, First, Last, ADLine1, ADLine2):
		self.First	= First
		self.Last	= Last
		self.ADLine1	= ADLine1
		self.ADLine2	= ADLine2

	def dumpAddress(self):
		print "Address Info:"
		print self.First, " ", self.Last
		print self.ADLine1, "\n", self.ADLine2

class DocumentText:
	def __init__(self,address):
		self.address = address

		self.header = ("\\documentclass[12pt]{article} \n"
			      +"\\usepackage{geometry} \n"
			      +"\\geometry{hmargin={1in,1in},vmargin={2in,1in}}\n"
			      +"\\begin{document} \n"
			      +"\\thispagestyle{empty} \n\n ")

		self.myAddress = "My Address \n\n\\vskip.5in \n\n"

		self.toAddress = (self.address.First + " " + self.address.Last + "\n\n"
				 + self.address.ADLine1 + "\n\n"
				 + self.address.ADLine2 + "\n\n")

		self.date      = "\\vskip.5in \n \\today \n\n \vskip.5in \n\n"

		self.greeting  = "Dear " + self.address.First + " " + self.address.Last + ", \n\n \vskip.5in"

		self.body      = ("I would like to thank you for being awesome and adding so much magic to the world"
			        + ". Please dont have anyone look at me."
			        + "Lets all hang out"
				+ "\n\n \vskip.5in \n\n \\hskip3.5in Casey \n\n"
				+ "\\hskip3.5in Stacey \n\n")

		self.footer   = "\\end{document}\n"

	def WriteLetter(self):
	    LetterOutName = str(self.address.last.lstrip(' ')) + ".tex"
	    LetterFile = open(LetterOutName,"w")
	    LetterFile.write(self.header)
	    LetterFile.write(self.myAddress)
	    LetterFile.write(self.toAddress)
	    LetterFile.write(self.date)
	    LetterFile.write(self.greeting)
	    LetterFile.write(self.body)
	    LetterFile.write(self.footer)
	    LetterFile.close()
