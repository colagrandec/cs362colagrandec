#!/bin/bash


HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Your Options Back Title"
TITLE="BigTimeOptions"
MENU="You Must Choose!"

OPTIONS=(1 "Options 1"
         2 "Options 2"
         3 "Options 3")
 

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)


clear
case $CHOICE in 
    1)
        echo "You chose Option 1"
        ;;
    2)
        echo "You chose Option 2"
        ;;
    3)  echo "Darth vador came to your house."
        ls
        echo "He looked at your files"
        ;;
esac

